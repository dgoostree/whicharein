package com.goostree.codewars.whicharein;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class WhichAreIn {
	public static String[] inArray(String[] array1, String[] array2) {
		Set<String> resultSet = new HashSet<String>();
		String[] resultArray;
		
		//for each string
		for(String str : array1) {
			
			//see if any in array2 contain it as a substring
			for(String otherStr : array2) {
				if(otherStr.contains(str)){
					resultSet.add(str); //add it to our set
					break;
				}
			}
			
		}
		resultArray = resultSet.toArray(new String[resultSet.size()]);
		Arrays.sort(resultArray); //let arrays sort it
		
		/*//Java 8
		return Arrays.stream(array1)
			      .filter(str ->
			        Arrays.stream(array2).anyMatch(s -> s.contains(str)))   //invoking stream(array2) for each element of array1? bleh
			      .distinct()
			      .sorted()
			      .toArray(String[]::new);*/

		
		return resultArray;		
	}
}
